UPDATE BANDANA
SET BANDANAVALUE = replace(BANDANAVALUE, 'confluence.teamsinspace.com', 'change.teamsinspace.com')
WHERE BANDANACONTEXT = '_GLOBAL'
AND BANDANAKEY = 'atlassian.confluence.settings';